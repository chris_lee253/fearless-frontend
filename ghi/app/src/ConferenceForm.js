import React, {useEffect, useState} from 'react';

function ConferenceForm(props) {
    const [name, setName] = useState('')
    const [startDate, setStart] = useState('')
    const [endDate, setEnd] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setPresentation] = useState('')
    const [maxAttendees, setAttendees] = useState('')
    const [location, setLocation] = useState('')
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        setName('');
        setStart('');
        setEnd('');
        setDescription('');
        setPresentation('');
        setAttendees('')
        setLocation('')
    }
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setPresentation(value);
    }
    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setAttendees(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const [locations, setLocations] = useState([])
    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)
    //   const selectTag = document.getElementById('state');
    //   for (let state of data.states) {
    //     const option = document.createElement('option');
    //     option.value = state.abbreviation;
    //     option.innerHTML = state.name;
    //     selectTag.appendChild(option);
    //   }
    }
  }

  useEffect(() => {
     fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={startDate} placeholder="Starts" required type="date" name="starts" id="start" className="form-control" />
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={endDate} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">End date</label>
              </div>
              <div className="mb-3">
                <textarea onChange={handleDescriptionChange} value={description} required placeholder="Description" name="description" id="description" className="form-control" cols="30" rows="10"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendeesChange} value={maxAttendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name= "location" id="location" className="form-select">
                    <option value="">Location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.name} value={location.id}>
                                {location.name}
                            </option>
                            );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
   );
  }

export default ConferenceForm;
