import React from 'react'; //imports react library
import ReactDOM from 'react-dom/client'; //imports reactDom Library
import './index.css'; //imports css styling
import App from './App'; //imports the App function from the App.js
import reportWebVitals from './reportWebVitals';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render( //root and rootrender is telling React where to take that HTML and put it.
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
        {/* this set a variable named attendees to the value inside the curly braces */}
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadAttendees();

//<App/> is what runs the apps function in the other file and whatever
// the JSX the function returns gets put there. Thats how it ends up building the HTML page.

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
